import tkinter as tk
#################################
#                               #
#       tkinter                 #
#       SCROLLING FRAME         #
#       by Enrico Galli         #
#       http://www.egw.it       #
#                               #
#################################

LOG = True


def log(msg):
    if LOG:
        print(msg)


class ScrollingFrameSlider:
    def __init__(self, target, orientation, slider_config):

        self.target = target
        self.orientation = orientation

        # configurazione di default della barra di scorrimento
        self.slider_config = {
            "width": 25,
            "height": 100,
            "border": 0,
            "bg": "#f2f2f2",
            "fill": "#999999",
            "activefill": "red",
            "outline": "",
            "cursor": "hand2"
        }

        # oggetti slider ed area slider
        self.slider = None
        self.area = None

        self.created = False
        self.position = []

        self.area_coords = []
        self.offset = 0

        self.y_extracontent = 0
        self.stepsize = 0

        # se viene passata una configurazione sostituisce quella di default con quella custom
        if slider_config:  # TODO sostituire con un loop
            self.slider_config = slider_config

    def initializeSlider(self):
        log("INITIALIZE SLIDER")
        ox = self.target.width - self.slider_config["width"]
        oy = 0
        ex = self.target.width
        ey = self.target.height

        self.updateCoords(ox, oy, ex, ey)

        return ox, oy, ex, ey

    def updateCoords(self, ox, oy, ex, ey):
        self.position = [ox, oy]
        self.area_coords = [ox, oy, ex, ey]
        self.updateStepSize()

    def updateStepSize(self):
        ox, oy, ex, ey = self.area_coords
        self.target.content_height = self.target.content.winfo_height()
        self.y_extracontent = self.target.content_height - (ey - oy)
        self.stepsize = self.y_extracontent / (ey - oy - self.slider_config["height"])


class ScrollingFrame(tk.Frame):
    def __init__(self, target, slider_config=None, **kwargs):
        super(ScrollingFrame, self).__init__(target, **kwargs)

        self.canvas = tk.Canvas(self, bg=self["bg"],  highlightthickness=0, width=self["width"])
        self.canvas.pack(anchor=tk.W, expand=True, fill=tk.BOTH, padx=0, pady=0)

        self.canvas.bind("<Enter>", lambda e: self.getFocus(True))
        self.canvas.bind("<Leave>", lambda e: self.getFocus(False))

        self.yslider = ScrollingFrameSlider(self, "y", slider_config)
        self.xslider = ScrollingFrameSlider(self, "x", slider_config)

        # self.content = tk.Frame(self.canvas, , width=50, borderwidth=1, border=1)
        _width: int = self["width"]-self.yslider.slider_config["width"]
        self.content = tk.Frame(self.canvas, bg=self["bg"], width=_width, padx=0, pady=0)
        self.content.place(x=0, y=0, width=_width)
        self.content.bind("<Configure>", lambda e: self.contentUpdate())

        self.slider_selected = False

        self.width = 0
        self.height = 0
        self.content_width = 0
        self.content_height = 0

        self.content_x = self.content.winfo_x()
        self.content_y = self.content.winfo_y()

    def getFocus(self, focus):
        if focus:
            self.canvas.bind_all("<Button-4>", lambda e: self.wheelContent("down"))
            self.canvas.bind_all("<Button-5>", lambda e: self.wheelContent("up"))
            self.canvas.bind_all("<Button-1>", lambda e: self.sliderSelected(True, e))
            self.canvas.bind_all("<ButtonRelease-1>", lambda e: self.sliderSelected(False, e))
            self.canvas.bind_all("<Motion>", lambda e: self.moveContent(e))
        else:
            log("OUT FOCUS")
            # self.sliderSelected(False)
            self.canvas.unbind_all("<Button-4>")
            self.canvas.unbind_all("<Button-5>")
            # self.canvas.unbind_all("<Button-1>")
            # self.canvas.unbind_all("<ButtonRelease-1>")
            # self.canvas.unbind_all("<Motion>")

    def contentUpdate(self):

        self.width = self.winfo_width()
        self.height = self.winfo_height()
        self.content_width = self.content.winfo_width()
        self.content_height = self.content.winfo_height()

        if self.content_height > self.height:
            if not self.yslider.created:

                ox, oy, ex, ey = self.yslider.initializeSlider()

                self.yslider.area = self.canvas.create_rectangle(
                    ox, oy, ex, ey, fill=self.yslider.slider_config["bg"], outline=""
                )

                self.yslider.slider = self.canvas.create_rectangle(
                    ox, oy, ox + self.yslider.slider_config["width"], oy+self.yslider.slider_config["height"],
                    width=self.yslider.slider_config["border"],
                    fill=self.yslider.slider_config["fill"],
                    activefill=self.yslider.slider_config["activefill"],
                    outline=self.yslider.slider_config["outline"],
                    tags="yslider"
                )

                self.canvas.tag_raise("yslider")
                self.yslider.created = True

                self.addBindings("yslider")

            else:

                self.yslider.updateStepSize()

        else:
            if self.yslider.slider is not None and self.yslider.slider > 0:
                self.canvas.delete(self.yslider.area)
                self.canvas.delete(self.yslider.slider)
                self.yslider.slider = None
                self.yslider.area = None
                self.yslider.created = False
                self.content.place(x=0, y=0)

    def addBindings(self, slider_tag):

        self.canvas.tag_bind(slider_tag, "<Enter>", lambda e: self.useCursor(self.yslider.slider_config["cursor"]))
        self.canvas.tag_bind(slider_tag, "<Leave>", lambda e: self.useCursor(""))
        self.canvas.tag_bind(slider_tag, "<Button-1>", lambda e: self.sliderSelected(True, e))
        self.canvas.tag_bind(slider_tag, "<ButtonRelease-1>", lambda e: self.sliderSelected(False, e))
        self.canvas.tag_bind(slider_tag, "<Motion>", lambda e: self.moveContent(e))

    def wheelContent(self, direction):

        if self.yslider.created:

            wheelstep = 50
            if direction == "down":
                wheelstep = -50

            ox, oy = self.yslider.position  # posizione iniziale dello slider
            sa_ox, sa_oy, sa_ex, sa_ey = self.yslider.area_coords  # vertici area slider

            ex = ox + self.yslider.slider_config["width"]

            canvas_item_id = self.canvas.find_withtag("yslider")[0]
            self.canvas.tag_raise("yslider")

            noy = oy + wheelstep
            ney = noy + self.yslider.slider_config["height"]

            if noy < 1:
                noy = 0
                ney = noy+self.yslider.slider_config['height']
            else:
                if ney > sa_ey:
                    ney = sa_ey
                    noy = ney-self.yslider.slider_config['height']

            self.canvas.coords(canvas_item_id, ox, noy, ex, ney)

            self.content.place(x=0, y=-noy * self.yslider.stepsize)
            self.yslider.position = [ox, noy]

    def sliderSelected(self, selected, event=None):

        if self.yslider.created:
            if selected:
                print("SLIDER SELECTED")
                ox, oy = self.yslider.position

                y = event.y

                self.yslider.offset = y - oy
            else:
                print("SLIDER UNSELECTED")
                canvas_item_id = self.canvas.find_withtag("yslider")[0]
                self.canvas.tag_raise("yslider")

                ox, oy, *_ = self.canvas.coords(canvas_item_id)
                self.yslider.position = [ox, oy]
                print("OUT", ox, oy)

            self.slider_selected = selected

    def moveContent(self, event):
        if self.yslider.created and self.slider_selected:

            ox, oy = self.yslider.position  # posizione iniziale dello slider
            sa_ox, sa_oy, sa_ex, sa_ey = self.yslider.area_coords  # vertici area slider

            ex = ox + self.yslider.slider_config["width"]

            canvas_item_id = self.canvas.find_withtag("yslider")[0]
            self.canvas.tag_raise("yslider")

            y = event.y

            noy = y - self.yslider.offset
            ney = noy + self.yslider.slider_config["height"]

            print(noy)

            if noy >= sa_oy and ney <= sa_ey:
                self.canvas.coords(canvas_item_id, ox, noy, ex, ney)
                self.content.place(x=0, y=-noy*self.yslider.stepsize)

    def useCursor(self, cursor=""):
        self.config(cursor=cursor)

    def getCoords(self):
        self.width = self.winfo_width()
        self.height = self.winfo_height()
        self.content_width = self.content.winfo_width()
        self.content_height = self.content.winfo_height()
