import tkinter as tk
from tkinter import font
from scrolling_frame import ScrollingFrame


def clear():
    for child in frame.content.winfo_children():
        child.destroy()

    frame.content.config(height=1)


def add():

    btn = tk.Label(
        frame.content,
        text="Lorem ipsum dolor sit amet, consectetur"
             "adipiscing elit, sed do eiusmod tempor"
             "incididunt ut labore et dolore magna aliqua."
             "Ut enim ad minim veniam, quis nostrud "
             "exercitation ullamco laboris nisi ut aliquip"
             "ex ea commodo consequat. Duis aute irure "
             "dolor in reprehenderit in voluptate velit "
             "esse cillum dolore eu fugiat nulla pariatur. "
             "Excepteur sint occaecat cupidatat non "
             "proident, sunt in culpa qui officia deserunt"
             "mollit anim id est laborum.",
        justify="left",
        bg="white",
        fg="#999999",
        font=font.Font(size=10),
        wraplength=500
    )
    btn.pack(anchor=tk.W, padx=10, pady=(10, 0))


root = tk.Tk()
root.geometry("1200x800")

frame = ScrollingFrame(root, bg="white", width=620)
frame.pack(side="right", expand=True, fill=tk.Y, pady=30, padx=30)


tk.Button(root, text="Add text", command=add, font=("arial", 9, "")).pack(anchor=tk.NW, padx=30, pady=30)
tk.Button(root, text="Clear", command=clear, font=("arial", 9, "")).pack(anchor=tk.SW, padx=30, pady=30)

for i in range(5):
    add()
root.mainloop()
